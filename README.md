# Contents of the repository

This project contains the specification and common XSD schemas for the Software Engineering project conducted by the Faculty of Mathematics and Information Sciences of Warsaw University of Technology.

# Language

The working language of the repository is English. Please use it in all the issues and commit comments.