\documentclass[oneside]{book}

\usepackage[lmargin=1.5in]{geometry}
 
\usepackage{graphicx}
\usepackage{ifpdf}
\usepackage[utf8]{inputenc}
\usepackage[OT4]{fontenc}
\usepackage{todonotes}
\usepackage{listings}
\usepackage{color}
\definecolor{gray}{rgb}{0.4,0.4,0.4}
\definecolor{darkblue}{rgb}{0.0,0.0,0.6}
\definecolor{cyan}{rgb}{0.0,0.6,0.6}

\lstset{
  basicstyle=\ttfamily,
  columns=fullflexible,
  showstringspaces=false,
  commentstyle=\color{gray}\upshape,
  frame=single
}

\lstdefinelanguage{XML}
{
  morestring=[b]",
  morestring=[s]{>}{<},
  morecomment=[s]{<?}{?>},
  stringstyle=\color{black},
  identifierstyle=\color{darkblue},
  keywordstyle=\color{cyan},
  morekeywords={xmlns,version,type}% list your attributes here
}

\ifpdf
	\usepackage[pdftex,unicode]{hyperref}
\else
	\usepackage[unicode]{hyperref}
\fi

\usepackage{adjustbox}

\begin{document}
\begin{frontmatter}
% Article top matter
\title{{\small{Software Engineering 2}}\\
\textbf{Project Game System}\\
		ver. 1.0 (release for 2017)}
\author{Micha\l{} Okulewicz}
\date{\today}
\maketitle
 
\tableofcontents

\section*{Glossary}
\begin{description}
    \item[The Project Game] - a real-time board game played by two competing teams of cooperating players
	\item[Player] - an agent playing the game, holds its own view of the state of the game
    \item[Team] - group of \textit{Players} who cooperate in order to achieve the goal of the game
    \item[Piece] - a token representing a project resource which is initially located in the \textit{Tasks Area} of the board by the \textit{Game Master},
	\item[Goal Field] - a type of field modelling a single project objective
	\item[Goal Area] - a part of the board where the \textit{Players} of a \textit{Team} have exclusive access and \textit{Goal Fields} are located (not all fields in the \textit{Goal Area} are \textit{Goal Fields})
    \item[Tasks Area] - a part of the board from which the \textit{Players} may collect the \textit{pieces}
    \item[Game Goal] - discovering the all the \textit{Goal Fields} by placing pieces in the fields of the \textit{Goal Area}
    \item[Project Game System] - a distributed IT system for managing multiple \textit{Project Games} and the agents participating in them
	\item[Game Master] - an agent responsible for generating the board and location of the \textit{Goal Fields}, holds the whole state of the game and generates new pieces in the \textit{Tasks Area},
	\item[Communications Server] - a module responsible for passing messages between \textit{Game Master} and \textit{Players}
\end{description}


\section*{Introduction}
The purpose of this document is to present the rules and the goal of The Project Game and specify the requirements for multi-agent system for playing the game.

The purpose of The Project Game itself is to create an environment in which computer and human agents might interact and whose behaviour can be observed, quantified and simulated.
The scope of this particular system is focused on the message passing system and computer agents only.

The system can be seen from the following perspectives:
\begin{itemize}
	\item as a network system,
	\item as a distributed database system,
	\item as a multi-agent system.
\end{itemize}

\subsection*{Network system perspective}
The Players are separated from each other and from Game Master by a network connection.
In the network system perspective, the goal of the project is to implement transparent communication between Players and Game Master agents.
The technical part of the communication should be properly separated from the logic through the message objects management and communication management layers.

\subsection*{Distributed database perspective}
Game Master holds the whole and most accurate state of the game (master database) and acts as a module for timestamping the data.
Players keep partial local copies of the state of the game (partial mirror databases).
Access to master database is limited to 9 fields per request,
while access to the data in mirror databases is limited by the accessibility of those nodes and the state of their data.
In the distributed system perspective, the goal of the project is to implement a best strategy for obtaining the most accurate and crucial data with as little cost as possible.

\subsection*{Multi-agent perspective}
Each Player forms beliefs from the knowledge obtained from the Game Master and other Players. Each Player assumes certain intentions of the Players in his Team and the opposite Team.
In the multi-agent perspective, the goal of the project is to implement the best strategy for obtaining, testing, placing the pieces and exchanging information
resulting in the fastest possible winning the game.

\subsection*{Document contents}
Part \ref{part:game} of the document describes the game organization, the objects appearing in the game, the possible moves of the players and their effects.
Part \ref{part:system} specifies the system technical requirements and defines the communication protocol.
\end{frontmatter}
\begin{mainmatter}
\part{The Project Game}
\label{part:game}
\chapter{Game description}

\section{Game concept}
The idea of The Project Game is to simulate competitive project development
by two teams of players.
The game simulates the following properties
of projects development: (1) tasks are connected with risks
(usually negative), (2) goals of the project are unclear
(and need to be discovered), and (3) communication
between members helps to speed up the process of the development.

\section{Game rules}
\begin{figure}[!ht]
	\centering
	\includegraphics[width=0.7\textwidth]{img/GameStateGlobal}
	\caption{Global state of the game.
	\label{fig:global-state}}
\end{figure}

The rules of the game could be summarized in the following way:
\begin{enumerate}
	\item The game is played by two teams of players: red and blue.
	\item The game is played on a rectangular board\footnote{the size of the board may vary} (depicted in Figure~\ref{fig:global-state}).
	\item The board is divided into 3 parts: a common tasks area, a red team goals area and a blue team goals area.
	\item The game is controlled by a game master, who places (in secret and randomly) pieces on the tasks area.
	\item The player has ability to move around the board, discover the state of surrounding fields, handle the pieces and exchange information with other players.
	\item The game is played in real time with a time cost assigned to each of the possible player's actions.
	\item The objective of the team is to complete the project as fast as possible by discovering all the goal fields in the goals area. 
	\item The goal fields are known only to the game master, a player who discovered them
and the players with whom that information has been shared.
	\item Player may carry only a single piece and a field in the tasks area may contain only a single piece\footnote{Player cannot place a piece on a field containing a piece, but Game Master can do it and destroy the piece which previously has been in the field}.
\end{enumerate}

\section{Game state}
The state of The Project Game consists of the following data:
\begin{itemize}
	\item Players' locations
	\item Pieces' locations
	\item Pieces' state (sham or non-sham)
	\item Project's goals locations
	\item Project's goals discovery state
\end{itemize}%
%
The true state of the game is known only to the Game Master.
Figure~\ref{fig:global-state} presents the state of the game as seen by the Game Master. 

\section{Game actions}
The Players discover the state of the game by interacting with the board,
the pieces and each other.
State of the game perceived by one of the blue Players before and after exchanging information with another blue Player is depicted in Figure~\ref{fig:personal-state}.

Possible moves of the Player consist of:
\begin{itemize}
	\item moving in one of 4 directions (up, down, right and left),
	\item discovering the contents of 8 neighbouring fields (and the currently occupied field),
	\item picking up a piece from a board,
	\item testing the picked piece for being a sham,
	\item placing a piece in the field in the goal area, in hope of completing one of the project objectives (or leaving a sham piece on the board),
	\item request exchange of information with another player.
\end{itemize}%
%
The Player may discover the goals only by placing (using) a piece in a given field of the goal area:
\begin{itemize}
	\item A correctly placed piece results in an information to the Player
that one of the project's goals has been completed (field has been a goal field).
	\item Incorrectly placed piece results in an information that the completed
action (getting the piece from the tasks board and placing
it in the goals area) has been meaningless, in the sense of project completion (field has not been a goal field).
	\item Placing a piece which is a sham results in getting no information (status of the field remains unknown to the player).
\end{itemize}%
%
Player actions have following ramifications and constrains:
\begin{itemize}
	\item Player cannot move into a field occupied by another player.
	\item Player cannot move into the goals area of the opposing team.
	\item The piece may be picked only by a Player which is located in the same field as that piece.
	\item Player can handle only one piece (he cannot pick up another piece).
	\item Player can place piece only on an empty field (with no other piece on it).
	\item Observing a field (either by discovering or entering it) results in receiving information about the Manhattan distance to the nearest piece.
	\item Team leader must exchange information with another Player from his team.
	\item Member of a team must exchange information with his team leader.
\end{itemize}
%
\begin{figure}[t]
	\centering
	\includegraphics[width=0.37\textwidth]{img/GameStatePersonal}
	\includegraphics[width=0.37\textwidth]{img/GameStatePersonalMerged}
	\caption{Personal game state perceived by one of the blue players.
	The left one is before he exchanged knowledge with the other
	blue player, while the right one after the exchange.
	Light blue color marks the traces of each of the clients.
	Visible piece has been added by Game Master during 
	player movement.
	\label{fig:personal-state}}
\end{figure}

\part{The Project Game System}
\label{part:system}
\chapter{Requirements}
The system organizes matches between teams of cooperating
agents.
The game is played in real time on a rectangular board, consisting
of the tasks area and the goals area.
Each team of agents needs to complete an identical project, consisting
of placing a set of pieces, picked from the tasks area, in the project goals area.
The pieces appear at random within the tasks area.
The agent's view of the tasks area is limited and the shape
of the goals needs to be discovered.
The game is won by the team of agents which is first to complete the project.
\section{Features}
All communication passes through Communication Server and uses TCP sockets.
Game Master serves one game at a time.
After completing one game Game Master and Players immediately register for another game (with the same settings).
Game Master logs requests metadata and a victory or defeat event for each of the Players.
Displays their ranking and average response times after each game on a text console.

Each Player displays the whole state of the board on a text console
after completing the game.

All the components should be able to run in a text-mode only.
Graphical User Interface is unnecessary, but may be additionally developed
as a means of presenting the game state for development or testing purpose.

\section{Supportability}
Game Master should log the following data after receiving any type of request from a player:
\begin{itemize}
	\item Request type (name of a root element in the XML message)
	\item Timestamp (date and time of receiving the request)
	\item Game id
	\item Player id
	\item Player GUID
	\item Player colour
	\item Player role
\end{itemize}
Game Master should also place an event winning/losing game event in the same log (marked as Victory or Defeat for each of the players). The log should be stored in an UTF-8 encoded CSV file located in the same directory as the binary files of the Game Master.

A sample part of the event log should look as follows:\\
\texttt{
\small
\begin{tabular}{lllllll}
Type & Timestamp & Game ID & Player ID & Player GUID & Colour & Role \\\hline
TestPiece & 2017-03-21T23:12:01.456 & 1 & 2 & c094\ldots & blue & member \\
Move & 2017-03-21T23:12:02.140 & 1 & 2 & c094\ldots & blue & member \\
Move & 2017-03-21T23:12:03.241 & 1 & 2 & c094\ldots & blue & member \\
Move & 2017-03-21T23:12:02.211 & 1 & 1 & c179\ldots & red & leader \\
PlacePiece & 2017-03-21T23:12:03.456 & 1 & 2 & c094\ldots & blue & member \\
Victory & 2017-03-21T23:12:03.456 & 1 & 2 & c094\ldots & blue & member \\
Defeat & 2017-03-21T23:12:03.456 & 1 & 1 & c179\ldots & red & leader \\
\end{tabular}
}

\section{Performance}
Communication Server and Game Master are able to serve smoothly at least 64 Players.
Theoretical number of Players is unlimited.
Communication Server is able to serve at least 8 independent games.
Theoretical number of games is unlimited.

The size and number of messages should be minimal.

\section{Security}
Player should be unable to get information available only to other players.
Player should not try to influence other Players' decisions with false information.

\section{Technical settings}
All the technical settings are set through an XML configuration file.

The following parameters are set in all the components:
\begin{itemize}
	\item Interval between keep alive bytes (or expected keep alive)
\end{itemize}%
%
The following parameters are set in the Player agent:
\begin{itemize}
	\item Interval between retries for joining the game with a given name.
\end{itemize}

\section{Game settings}
All the game settings are set through an XML configuration file.

In order to create various types of games the following elements of the game are configurable as the parameters of the Game Master agent:
\begin{itemize}
	\item Probability of piece being a sham
	\item Frequency of placing new pieces on board
	\item Initial number of pieces on board
	\item Width of the board, length of the tasks area, length of a single goals area
	\item Number of players in each of the teams
	\item Goal definition
	\item Name of the game
\end{itemize}

In order to balance different actions of the players, response delay for each of the actions is configured as the parameters of the Game Master agent (with default values given in parenthesis):
\begin{itemize}
	\item Move delay (100ms)
	\item Discovery delay (450ms)
	\item Test delay (500ms)\footnote{The idea: should take as long as crossing half of the board.}
	\item Pick-up delay (100ms)
	\item Placing delay (100ms)
	\item Knowledge exchange (1200ms)\footnote{The idea: should take as long as discovering number of the board task fields divided by number of players in team.}
\end{itemize}

\section{Running the application}
Apart from the configuration files the following parameters are passed while starting the application and a proper shell script / batch file accepting them, and running certain modules of the system, must be dispatched with the project.

\begin{itemize}
	\item YY - the current year (17)
	\item LANG - language (PL or EN)
	\item XX - group identifier
\end{itemize}

\noindent Communication Server runtime parameters:\\
\texttt{YY-LANG-XX-cs  --port [port number binded on all interfaces]
--conf [configuration filename]}

\noindent Player and Game Master runtime parameters:\\
\texttt{YY-LANG-XX-[pl|gm] --address [server IPv4 address or IPv6 address or host name] --port [server port number] --conf [configuration filename]} 

In order to join a particular type of a game in a particular role the following elements are configurable as the parameters of the Player agent:
\begin{itemize}
	\item Name of the game to join
	\item Preferred colour
	\item Preferred role
\end{itemize}
The Player will play only the game named exactly as requested, but the colour and the role might be assigned by Game Master as needed.
For example, if the game has already all the necessary blue players,
and there are new players coming, who prefer to be blue,
they would still be given a vacant role in a red team, as long as the specified name of the game has a matches a name registered by a Game Master.

Therefore, apart from the technical setting the Player has additional parameters:\\
\texttt{--game [name of the game] --team [red|blue] --role [leader|player]}

\chapter{Communication protocol}
The communication is maintained through the TCP/IP protocol.
All messages are passed through the Communication Server
which acts as the service for registering and deregistering the games
and as a proxy for passing all other messages.

The specification presents the exchange of messages between the components
for the following typical scenarios: registering and starting a game, normal gameplay, information exchange between the players.

All messages are text XML messages.
The messages are separated by a bytecode 23 (ETB - End transmission blocks).
The code is present after each message and is also used
as a connection keep alive data.
The TCP/IP connections are maintained during the whole time
of system operations, until the process, of at least one of the components 
in that particular connection, is finished.
Keep alive byte is sent from Player and Game Master to the Communication Server
and responded with a keep alive byte. Sending a normal message also acts as keep alive.

In order for the Communication Server to be able to host multiple games
at the same time, all messages send from Players and Game Master intended for a given Player or a Game Master are marked with the \texttt{playerId}
or \texttt{gameId}.
In order to provide security to the player's knowledge
each action message intended for the Game Master
has a unique player GUID known only to that Player and Game Master.
%(How to make system secure? Maybe server wraps up the ID?)

\section{Starting a game}
In order to start a new game Game Master must register the game on the Communication Server by \texttt{RegisterGame} message.
When Players discover the existence of game by sending \texttt{GetGames} message,
they subsequently choose the game they wish to join by sending \texttt{JoinGame} message.
After completing all the Players, Game Master broadcasts the \texttt{Game} message
to all Players, containing information about the size of the board, their teams and their initial location.
The exchange of those messages and their responses is presented in Fig.~\ref{fig:start.game}.

\begin{figure}
\centering
	\includegraphics[scale=1.0,page=1]{img/Communication}
	\caption{\label{fig:start.game}
	The messages passed between a sample (P)layer, (G)ame (M)aster and (C)ommunication (S)erver when a new game is organized in the system.}
\end{figure}

\begin{figure}
%\begin{adjustbox}{width=\textwidth,keepaspectratio}
\lstset{language=XML}
\begin{lstlisting}
<?xml version="1.0" encoding="utf-8" ?>
<GetGames xmlns="https://se2.mini.pw.edu.pl/17-results/" />
\end{lstlisting}
%\end{adjustbox}
\caption{An example of \texttt{GetGames} message}
\end{figure}

\begin{figure}
%\begin{adjustbox}{width=\textwidth,keepaspectratio}
\lstset{language=XML}
\begin{lstlisting}
<?xml version="1.0" encoding="utf-8"?>
<RegisterGame xmlns="https://se2.mini.pw.edu.pl/17-results/">
  <NewGameInfo
  	name="easyGame"
  	blueTeamPlayers="2"
  	redTeamPlayers="2" />
</RegisterGame>
\end{lstlisting}
%\end{adjustbox}
\caption{An example of \texttt{RegisterGame} message with a custom name and a two players teams setup.}
\end{figure}

\begin{figure}
%\begin{adjustbox}{width=\textwidth,keepaspectratio}
\lstset{language=XML}
\begin{lstlisting}
<?xml version="1.0" encoding="utf-8" ?>
<ConfirmGameRegistration
	xmlns="https://se2.mini.pw.edu.pl/17-results/"
	gameId="1" />
\end{lstlisting}
%\end{adjustbox}
\caption{An example of \texttt{ConfirmGameRegistration} message assigning id 1 to the game.}
\end{figure}

\begin{figure}
%\begin{adjustbox}{width=\textwidth,keepaspectratio}
\lstset{language=XML}
\begin{lstlisting}
<?xml version="1.0" encoding="utf-8" ?>
<RegisteredGames xmlns="https://se2.mini.pw.edu.pl/17-results/">
  <!-- Numbers of players indicate how many slots are left for each team -->
  <GameInfo gameName="Easy game" blueTeamPlayers="2" redTeamPlayers="2"/>
  <GameInfo gameName="Hard for blue game" blueTeamPlayers="5" redTeamPlayers="10"/>
</RegisteredGames>
\end{lstlisting}
%\end{adjustbox}
\caption{An example of \texttt{RegisteredGames} message with two games listed.}
\end{figure}

\begin{figure}
%\begin{adjustbox}{width=\textwidth,keepaspectratio}
\lstset{language=XML}
\begin{lstlisting}
<?xml version="1.0" encoding="utf-8" ?>
<JoinGame xmlns="https://se2.mini.pw.edu.pl/17-results/" 
  gameName="Easy game"
  preferedRole="leader"
  preferedTeam="blue" />
\end{lstlisting}
%\end{adjustbox}
\caption{A \texttt{JoinGame} message with player trying to join, as the leader of a blue team, the game denoted as \textit{easyGame}.}
\end{figure}

\begin{figure}
%\begin{adjustbox}{width=\textwidth,keepaspectratio}
\lstset{language=XML}
\begin{lstlisting}
<?xml version="1.0" encoding="utf-8" ?>
<ConfirmJoiningGame xmlns="https://se2.mini.pw.edu.pl/17-results/"
                    gameId="1"
                    playerId="2"
                    privateGuid="c094cab7-da7b-457f-89e5-a5c51756035f">
  <PlayerDefinition id="2" team="blue" type="member"/>
</ConfirmJoiningGame>
\end{lstlisting}
%\end{adjustbox}
\caption{A \texttt{ConfirmJoiningGame} message setting the players unique Id and private GUID and informing about the Player's role in the game.}
\end{figure}

\begin{figure}
%\begin{adjustbox}{width=\textwidth,keepaspectratio}
\lstset{language=XML}
\begin{lstlisting}
<?xml version="1.0" encoding="utf-8" ?>
<RejectJoiningGame xmlns="https://se2.mini.pw.edu.pl/17-results/"
                    gameName="Easy game"
                    playerId="2"
                    />
\end{lstlisting}
%\end{adjustbox}
\caption{A \texttt{RejectJoiningGame} message informing that Game Master rejects player.}
\end{figure}

\begin{figure}
%\begin{adjustbox}{width=\textwidth,keepaspectratio}
\lstset{language=XML}
\begin{lstlisting}
<?xml version="1.0" encoding="utf-8"?>
<Game xmlns="https://se2.mini.pw.edu.pl/17-results/"
      playerId="2">
  <Players>
    <Player team="red" type="leader" id="5" />
    <Player team="red" type="member" id="6" />
    <Player team="red" type="member" id="7" />
    <Player team="red" type="member" id="8" />
    <Player team="blue" type="leader" id="1" />
    <Player team="blue" type="member" id="2" />
    <Player team="blue" type="member" id="3" />
    <Player team="blue" type="member" id="4" />
  </Players>
  <Board width="5" tasksHeight="5" goalsHeight="3" />
  <PlayerLocation x="0" y="3" />
</Game>
\end{lstlisting}
%\end{adjustbox}
\caption{A \texttt{GameMessage} for Player 2.}
\end{figure}

\section{Standard gameplay}
\begin{figure}
\centering
	\includegraphics[scale=1.0,page=3]{img/Communication}
	\caption{\label{fig:gameplay}
	The messages passed between sample Players P1,P2 and a (G)ame (M)aster during normal gameplay. The Communication Server is not depicted but acts as a proxy for communication.}
\end{figure}
During a standard gameplay Players send \texttt{Discover}, \texttt{Move}, \texttt{PickUp}, \texttt{TestPiece} and \textbf{Place} messages to the GameMaster.
The messages specify only the information necessary for the proxy server to be dispatched
to a proper Game Master (\texttt{gameId} field) and the data allowing to safely identify the Player (\texttt{playerGuid} field) and decide about the Player's action (direction in the case of movement).
All the other details (like the ID of the piece being picked up) are maintained by the Game Master only.
To each request Game Master responses with a \texttt{Data} message containing
the information obtained by the Player, concerning part of the game state.
After the move finishing the game GameMaster broadcasts \texttt{Data} message containing the full state of the game, with the info that the game has finished to all the Players.
The \texttt{Data} messages include the information allowing the server to dispatch the data to a proper player (\texttt{playerId} field) and the data about the game state aggregated in the \texttt{TaskFields}, \texttt{GoalFields},\texttt{Pieces} and \texttt{PlayerLocation} elements.

Please note, that the Manhattan distance to the nearest piece is calculated
to a nearest piece currently placed on a field in the Tasks Area,
regardless of its status (sham or not-sham).
If no such piece is currently in the game, the distance value is equal to $-1$.
The distance is calculated only for the fields in the Tasks Area,
but can be observed from a Goals Area if the discover action is applied while standing
on a field in the Goals Area neighbouring fields in the Tasks Area.

Figure~\ref{fig:gameplay} presents the messages exchange between Players and Game Master.
Please note, that the Communication Server \textbf{always acts as a proxy} in this communication,
but is not depicted.
Knowledge exchange between the Players, which is also a part of a gameplay, is explained in the next section.

\begin{figure}
%\begin{adjustbox}{width=\textwidth,keepaspectratio}
\lstset{language=XML}
\begin{lstlisting}
<?xml version="1.0" encoding="utf-8"?>
<Discover xmlns="https://se2.mini.pw.edu.pl/17-results/"
    gameId="1"
    playerGuid="c094cab7-da7b-457f-89e5-a5c51756035f"
    />
\end{lstlisting}
%\end{adjustbox}
\caption{A \texttt{Discover} message from Player.}
\end{figure}

\begin{figure}
%\begin{adjustbox}{width=\textwidth,keepaspectratio}
\lstset{language=XML}
\begin{lstlisting}
<?xml version="1.0" encoding="utf-8"?>
<Data xmlns="https://se2.mini.pw.edu.pl/17-results/"
      playerId="1"
      gameFinished="false" >
  <TaskFields>
    <TaskField x="1" y="4" timestamp="2017-02-23T17:20:11"
      distanceToPiece="1" />
    <TaskField x="1" y="5" timestamp="2017-02-23T17:20:11"
      distanceToPiece="0" playerId="2" pieceId="2" />
    <TaskField x="1" y="6" timestamp="2017-02-23T17:20:11"
      distanceToPiece="1" />
    <TaskField x="0" y="4" timestamp="2017-02-23T17:20:11"
      distanceToPiece="2" />
    <TaskField x="0" y="5" timestamp="2017-02-23T17:20:11"
      distanceToPiece="1" />
    <TaskField x="0" y="6" timestamp="2017-02-23T17:20:11"
      distanceToPiece="2" />
    <TaskField x="2" y="4" timestamp="2017-02-23T17:20:11"
      distanceToPiece="2" />
    <TaskField x="2" y="5" timestamp="2017-02-23T17:20:11"
      distanceToPiece="1" />
    <TaskField x="2" y="6" timestamp="2017-02-23T17:20:11"
      distanceToPiece="2" />
  </TaskFields>
</Data>
\end{lstlisting}
%\end{adjustbox}
\caption{A \texttt{Data} message response for the discover action.}
\end{figure}

\begin{figure}
%\begin{adjustbox}{width=\textwidth,keepaspectratio}
\lstset{language=XML}
\begin{lstlisting}
<?xml version="1.0" encoding="utf-8"?>
<Move xmlns="https://se2.mini.pw.edu.pl/17-results/"
      gameId="1"
      playerGuid="c094cab7-da7b-457f-89e5-a5c51756035f"
      direction="up"/>
\end{lstlisting}
%\end{adjustbox}
\caption{A \texttt{Move} message from Player.}
\end{figure}

\begin{figure}
%\begin{adjustbox}{width=\textwidth,keepaspectratio}
\lstset{language=XML}
\begin{lstlisting}
<?xml version="1.0" encoding="utf-8"?>
<Data xmlns="https://se2.mini.pw.edu.pl/17-results/"
      playerId="1"
      gameFinished="false">
  <TaskFields>
    <TaskField x="1" y="5" timestamp="2017-02-23T17:20:11" 
               distanceToPiece="5" />
  </TaskFields>
  <PlayerLocation x="1" y="5" />
</Data>
\end{lstlisting}
%\end{adjustbox}
\caption{A \texttt{Data} message response for the proper move action.}
\end{figure}

\begin{figure}
%\begin{adjustbox}{width=\textwidth,keepaspectratio}
\lstset{language=XML}
\begin{lstlisting}
<?xml version="1.0" encoding="utf-8"?>
<Data xmlns="https://se2.mini.pw.edu.pl/17-results/"
      playerId="1"
      gameFinished="false">
  <TaskFields>
    <TaskField x="1" y="5" timestamp="2017-02-23T17:20:11"
               distanceToPiece="0" playerId="2" pieceId="2" />
  </TaskFields>
  <Pieces>
    <Piece id="2" timestamp="2017-02-23T17:20:11" playerId="2"
           type="unknown" />
  </Pieces>
  <PlayerLocation x="1" y="4" />
</Data>
\end{lstlisting}
%\end{adjustbox}
\caption{A \texttt{Data} message response for the move action, when trying to enter an occupied field.}
\end{figure}

\begin{figure}
%\begin{adjustbox}{width=\textwidth,keepaspectratio}
\lstset{language=XML}
\begin{lstlisting}
<?xml version="1.0" encoding="utf-8"?>
<Data xmlns="https://se2.mini.pw.edu.pl/17-results/"
      playerId="1"
      gameFinished="false">
  <TaskFields>
  </TaskFields>
  <PlayerLocation x="1" y="7" />
</Data>
\end{lstlisting}
%\end{adjustbox}
\caption{A \texttt{Data} message response for the move action, while trying to step out of the board.}
\end{figure}

\begin{figure}
%\begin{adjustbox}{width=\textwidth,keepaspectratio}
\lstset{language=XML}
\begin{lstlisting}
<?xml version="1.0" encoding="utf-8"?>
<PickUpPiece
    xmlns="https://se2.mini.pw.edu.pl/17-results/"
    gameId="1"
    playerGuid="c094cab7-da7b-457f-89e5-a5c51756035f" />
\end{lstlisting}
%\end{adjustbox}
\caption{A \texttt{PickUp} message from a Player.}
\end{figure}

\begin{figure}
%\begin{adjustbox}{width=\textwidth,keepaspectratio}
\lstset{language=XML}
\begin{lstlisting}
<?xml version="1.0" encoding="utf-8"?>
<Data xmlns="https://se2.mini.pw.edu.pl/17-results/"
      playerId="1"
      gameFinished="false">
  <Pieces>
    <Piece id="2" timestamp="2017-02-27T12:00:34" playerId="1"
           type="unknown" />
  </Pieces>
</Data>
\end{lstlisting}
%\end{adjustbox}
\caption{A \texttt{Data} message response for the piece pick up action.}
\end{figure}

\begin{figure}
%\begin{adjustbox}{width=\textwidth,keepaspectratio}
\lstset{language=XML}
\begin{lstlisting}
<?xml version="1.0" encoding="utf-8"?>
<TestPiece xmlns="https://se2.mini.pw.edu.pl/17-results/"
      gameId="1"
      playerGuid="c094cab7-da7b-457f-89e5-a5c51756035f" />
\end{lstlisting}
%\end{adjustbox}
\caption{A \texttt{TestPiece} message from a Player.}
\end{figure}

\begin{figure}
%\begin{adjustbox}{width=\textwidth,keepaspectratio}
\lstset{language=XML}
\begin{lstlisting}
<?xml version="1.0" encoding="utf-8"?>
<Data xmlns="https://se2.mini.pw.edu.pl/17-results/"
      playerId="1"
      gameFinished="false">
  <Pieces>
    <Piece id="2" type="sham" playerId="1" timestamp="2017-02-28T13:45:56" />
  </Pieces>
</Data>
\end{lstlisting}
%\end{adjustbox}
\caption{A \texttt{Data} message response for the testing of a piece action.}
\end{figure}

\begin{figure}
%\begin{adjustbox}{width=\textwidth,keepaspectratio}
\lstset{language=XML}
\begin{lstlisting}
<?xml version="1.0" encoding="utf-8"?>
<PlacePiece xmlns="https://se2.mini.pw.edu.pl/17-results/"
      gameId="1"
      playerGuid="c094cab7-da7b-457f-89e5-a5c51756035f" />
\end{lstlisting}
%\end{adjustbox}
\caption{A \texttt{PlacePiece} message from a Player.}
\end{figure}

\begin{figure}
%\begin{adjustbox}{width=\textwidth,keepaspectratio}
\lstset{language=XML}
\begin{lstlisting}
<?xml version="1.0" encoding="utf-8"?>
<Data xmlns="https://se2.mini.pw.edu.pl/17-results/"
      playerId="1"
      gameFinished="false">
  <GoalFields>
    <GoalField x="0" y="0" team="red" playerId="1"
     timestamp="2017-02-28T14:56:21" type="non-goal"/>
  </GoalFields>
</Data>
\end{lstlisting}
%\end{adjustbox}
\caption{A \texttt{Data} message response for the placing of a piece in a goals area action.}
\end{figure}

\clearpage
\subsection{Knowledge exchange}
\begin{figure}
\centering
	\includegraphics[scale=1.0,page=2]{img/Communication}
	\caption{\label{fig:exchange}
	The messages passed between sample Players P1,P2 and a (G)ame (M)aster with possible scenarios for data exchange. The Communication Server is not depicted but acts as a proxy for communication.}
\end{figure}

Knowledge exchange is a special type of action during the gameplay as it involves not only a single Player and a Game Master, but also another Player. Therefore, it needs special handling as the cost of all actions need to be controlled and imposed by the Game Master.

The knowledge exchange is depicted in Fig.~\ref{fig:exchange}, with omission of the Communication Server as a proxy, and performed in a following way:
\begin{enumerate}
	\item \texttt{AuthorizeKnowledgeExchange} message is sent from Player 1 to the Game Mastered and relayed to the intended Player 2 as \texttt{KnowledgeExchangeRequest} message
	\item Player 1 might reject the offer (without further delay) by sending a \texttt{RejectKnowledgeExchange} to Player 2
	\item Player 2 might accept the offer by sending a \texttt{Data} message containing his whole state to Player 1 and an \texttt{AuthorizeKnowledgeExchange} message to Game Master
	\item Game Master relays the message as an \texttt{AcceptExchangeRequest} message, which cannot be refused, and results in a response \texttt{Data} message from Player 1 to Player 2.
\end{enumerate}

\begin{figure}
%\begin{adjustbox}{width=\textwidth,keepaspectratio}
\lstset{language=XML}
\begin{lstlisting}
<?xml version="1.0" encoding="utf-8"?>
<AuthorizeKnowledgeExchange xmlns="https://se2.mini.pw.edu.pl/17-results/"
  withPlayerId="2"
  gameId="1"
  playerGuid="c1797885-8773-43ea-b454-d78315341a02"
  />
\end{lstlisting}
%\end{adjustbox}
\caption{An \texttt{AuthorizeKnowledgeExchange} message.}
\end{figure}

\begin{figure}
%\begin{adjustbox}{width=\textwidth,keepaspectratio}
\lstset{language=XML}
\begin{lstlisting}
<?xml version="1.0" encoding="utf-8"?>
<KnowledgeExchangeRequest xmlns="https://se2.mini.pw.edu.pl/17-results/"
  playerId="2"
  senderPlayerId="1" />
\end{lstlisting}
%\end{adjustbox}
\caption{A \texttt{KnowledgeExchangeRequest} message.}
\end{figure}

\begin{figure}
%\begin{adjustbox}{width=\textwidth,keepaspectratio}
\lstset{language=XML}
\begin{lstlisting}
<?xml version="1.0" encoding="utf-8"?>
<RejectKnowledgeExchange xmlns="https://se2.mini.pw.edu.pl/17-results/"
  permanent="false"
  playerId="1"
  senderPlayerId="2" />
\end{lstlisting}
%\end{adjustbox}
\caption{A \texttt{RejectKnowledgeExchange} message.}
\end{figure}

\begin{figure}
%\begin{adjustbox}{width=\textwidth,keepaspectratio}
\lstset{language=XML}
\begin{lstlisting}
<?xml version="1.0" encoding="utf-8"?>
<AcceptExchangeRequest xmlns="https://se2.mini.pw.edu.pl/17-results/"
  playerId="1"
  senderPlayerId="2"
  />
\end{lstlisting}
%\end{adjustbox}
\caption{An \texttt{AcceptExchangeRequest} message.}
\end{figure}

\begin{figure}
%\begin{adjustbox}{width=\textwidth,keepaspectratio}
\lstset{language=XML}
\begin{lstlisting}
<?xml version="1.0" encoding="utf-8"?>
<Data xmlns="https://se2.mini.pw.edu.pl/17-results/"
      playerId="1"
      gameFinished="false">
  <TaskFields>
    <TaskField x="1" y="5" timestamp="2017-02-23T17:20:11"
               distanceToPiece="5" />
    <TaskField x="1" y="4" timestamp="2017-02-23T17:20:13"
               distanceToPiece="4" />
  </TaskFields>
  <GoalFields>
    <GoalField x="0" y="9" timestamp="2017-02-23T17:20:17"
               team="blue" type="non-goal"/>
    <GoalField x="1" y="9" timestamp="2017-02-23T17:20:19"
               team="blue" type="goal" playerId="2"/>
  </GoalFields>
  <Pieces>
    <Piece id="1" timestamp="2017-02-23T17:20:09" type="sham" />
    <Piece id="2" timestamp="2017-02-23T17:19:09" type="unknown" />
  </Pieces>
</Data>
\end{lstlisting}
%\end{adjustbox}
\caption{A \texttt{Data} message with a knowledge exchange/accept exchange response data.}
\end{figure}

\end{mainmatter}
\end{document}  %End of document.