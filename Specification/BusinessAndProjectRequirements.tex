\documentclass{article}
 
\usepackage{graphicx}
\usepackage{ifpdf}
\usepackage[utf8]{inputenc}
\usepackage[OT4]{fontenc}
\usepackage{todonotes}

\ifpdf
	\usepackage[pdftex,unicode]{hyperref}
\else
	\usepackage[unicode]{hyperref}
\fi

\begin{document}
 
% Article top matter
\title{\textbf{The Project Game}\\
		\small{Software Engineering 1}}
\author{Krzysztof Kaczmarski\\
		Micha\l{} Okulewicz}
\date{\today}
\maketitle
 
\begin{abstract}
This document describes the overview of the Project Game
system and the requirements for its written specification.
\end{abstract}

\section{Introduction}
The system organizes matches between teams of cooperating
agents.
The game is played in real time on a rectangular board, consisting
of the tasks area and the goals area.
Each team of agents needs to complete an identical project, consisting
of placing a set of pieces, picked from the tasks area, in the project goals area.
The pieces appear at random within the tasks area.
The agent's view of the tasks area is limited and the shape
of the goals needs to be discovered.
The game is won by the team of agents which is first to complete the project.

The architecture of a system and a communication protocol design should focus on solving the effects of asynchronous
actions taken by the agents.

 
\section{Glossary}
\begin{description}
    \item[Project Game] - a real-time board game played by a two competing teams of cooperating players
	\item[Game Master] - generates the board and the shape of the project goals, holds the real state of the game and generates new pieces on the tasks area
	\item[Player] - an agent playing the game, holds its own view of the state of the game
	\item[Communications Server] - responsible for passing messages between \textit{Game Master} and \textit{Players}
    \item[Team] - group of \textit{Players} who cooperate in order to achieve the goal of the game
    \item[Goal Area] - a part of the board where the \textit{Players} place the \textit{pieces}
    \item[Tasks Area] - a part of the board from which the \textit{Players} may collect the \textit{pieces}
    \item[Game Goal] - discovering the shape of the project goal by placing a set of pieces in the \textit{Goal Area}
    \item[Piece] - a token representing the project resource which is placed in the \textit{Tasks Area} of the board by the \textit{Game Master}, picked from the \textit{Tasks Area} by one of the~\textit{Players} and placed by that \textit{Player} in the \textit{Goal Area}
\end{description}

\section{Task}
The task is to design (till 23 January 2017) and develop (next semester) the architecture, components and communication protocol for the \textbf{Project Game system} and all of its components.

\section{Design requirements}
Design documentation is to be prepared by 4 people teams\footnote{If the total number of students is not divisible by 4, the minimum necessary amount of 3 people teams is allowed.} \footnote{Please account for the students who are currently abroad, but will return next semester.}.
It must be sent to the teacher via mail in PDF format
before the end of the semester. The first page
should contain a list of team members.
The design documentation is a written document
describing system components and their activities, with \textbf{additional}
clarifications presented in the form of the UML
diagrams.
All diagrams \textbf{must be} thoroughly commented
and explained.
Please note that diagrams must be created
with special treatment, to be readable both in black\&white print and on a computer screen\footnote{Usage of a LaTex system is highly recommended, because of its proper handling of the vector graphics (PDF, EPS or PS formats) in which the diagrams need to be prepared}. 

\subsection*{Required artifacts}
\begin{enumerate}
	\item Requirements Specification:
	\begin{enumerate}
		\item System functions described with the usage of the UML Use Case diagrams
		\item Non-functional requirements described in accordance with the FURPS+ model\footnote{
		\href{http://www.ibm.com/developerworks/rational/library/4706.html\#N100A7}
		{\texttt{http://www.ibm.com/developerworks/rational/library/4706.html\#N100A7}}\\
		\href{http://agileinaflash.blogspot.com/2009/04/furps.html}
		{\texttt{http://agileinaflash.blogspot.com/2009/04/furps.html}}
		}
		\item Components' runtime parameters / input data format specification
	\end{enumerate}
	\item Complete Design Documentation:
	\begin{description}
		\item[Architecture design] - system's main components depiction and description, with their methods of communication. Special attention should be paid to capturing the common system modules (which should be described with the use of UML Class Diagrams).
		\item[Communication protocol design] - system's messages definitions and the order in which they should be passed between components. The design should use UML Sequence Diagrams and some form of a formal message description (suggested method: XML Schema +
			examples of XML messages)
		\item[Special system states description]
			(initialization, shut down, communications server, player and game master failures) - in the case of such system special attention must be paid to the behaviour after components' failures (UML State and Activity Diagrams need to be applied).
		\item[Game related objects] - description of objects' features and methods necessary for describing the game rules and interactions (with the usage of UML Class Diagrams)
		\item[Game rules] - description of the board and player's states (with the usage of UML State Diagrams)
		\item[Game interactions] - description of the parts of the gameplay and agent player strategy (with the usage of UML  Activity Diagrams)
		\item[Additional relevant comments]
	\end{description}
	\item Glossary
\end{enumerate}

\section{Game concept}
The goal of the Project Game is to simulate project development
in a cooperation-and-competition multi-agent environment.
The environment should simulate the following properties
of projects development: (1) tasks are connected with risks
(usually negative), (2) goals of the project are unclear
(and need to be discovered), and (3) communication
between members helps to speed up the process.

\begin{figure}[!ht]
	\centering
	\includegraphics[width=0.7\textwidth]{img/GameStateGlobal}
	\caption{Global state of the game.
	\label{fig:global-state}}
\end{figure}

The risk of doing a task is simulated by a probability
that a given piece is a sham.
The agent can discover that fact either by testing
a piece (which it currently holds) or by placing it in the goal area and not receiving any feedback, about that field being a part of a project goal or not.

The difficulty of project is simulated by
the size of the goal area on board, the shape of the fields
in which the pieces need to be placed and the number
of~those fields.

The cooperation between the agents (players) is simulated
by the ability to exchange system states and having a common
goal.
Only through the communication agents can get access to the information about 
the project goals completed (or misinterpreted) by other agents.
The role of the leader is signified with the ability to exchange
information with any member of his team.
Exchange of information between other team members (or players
from different teams) needs a mutual consent.


\begin{figure}[t]
	\centering
	\includegraphics[width=0.37\textwidth]{img/GameStatePersonal}
	\includegraphics[width=0.37\textwidth]{img/GameStatePersonalMerged}
	\caption{Personal game state perceived by one of the blue players.
	The left one is before he exchanged knowledge with the other
	blue player, while the right one after the exchange.
	Light blue color marks the traces of each of the clients.
	Visible piece has been added by Game Master during 
	player movement.
	\label{fig:personal-state}}
\end{figure}

\section{Game rules}
The game is played on a board\footnote{size of the board is a game parameter} depicted in Figure~\ref{fig:global-state}.
That figure presents the state of the game as seen by the Game Master.
The true state of the fields in the goal areas is known only to the Game Master.
The Player can learn about them only by placing (using) a piece in a given field of the goal area:
\begin{itemize}
	\item A correctly placed piece results in an information to the Player
that one of the goals of the project have been completed.
	\item Incorrectly placed piece results in an information that the completed
action (getting the piece from the tasks board and placing
it in the goals area) has been meaningless, in the sense of project completion.
	\item Placing a piece which is a sham results in getting no information.
State of the game perceived by one of the blue Players is depicted in Figure~\ref{fig:personal-state}.
\end{itemize}
Possible moves of the Player consist of:
\begin{itemize}
	\item moving in one of 4 directions,
	\item discovering the contents of 8 neighbouring fields,
	\item testing the picked piece for being a sham,
	\item placing a piece in the goals, in hope of completing one of the project objectives,
	\item request exchange of information with another player.
\end{itemize}
Player actions have following ramifications and constrains:
\begin{itemize}
	\item The piece is picked up by a Player which moves into the piece's field first.
	\item Player cannot move into a field occupied by another player.
	\item Observing a field (either by discovering or entering it) results in receiving information about the Manhattan distance to the nearest piece.
\end{itemize}

\section{System concept}
The Project Game system consist of the three main components:
Players, Game Master and Communication Server.

The Players and the Game Master connect to the Communications Server.
Game Master is responsible for managing the game, while Communications
Server for managing the communication at the lower level by
passing the messages between the Players and Game Master
without processing their content.
The Players interact with the Game Master and one another
in order for their team to complete the game as fast as possible.

%  Game master:
% { sends game information to the server
% { receives list of players who participate this game
% { prepares game initial state
% { decides who makes the rst move
% { sends move request to the selected player (via server)
% { receives move from the server
% { sends game state and move to the server (forwarded then to all participants)
% { decides if the game is nished or not
% { in such case sends special end game message to the server
% { decides who makes the next move
% { etc.
%  Player:
% { receives a game state from the server
% { receives a move request from the server
% { makes move and sends it to the server
% { receives information about game nal result
%  Game server:
% { receives game state from game master
% { receives the number of next player to make a move
% { sends game state to players
% { sends move request to given player 
% { receives move from player
% { sends this move to game master

\end{document}  %End of document.